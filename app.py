from typing import Literal

from PyQt5.QtWidgets import QApplication, QPushButton, QMainWindow, QTextEdit


class Example(QMainWindow):
    def __init__(self, exception_type: Literal['usual', 'file']):
        super().__init__()
        self.exception_type = exception_type
        self.initUI()

    def initUI(self):
        self.setGeometry(600, 600, 600, 600)
        self.button = QPushButton('Создать проблемы', self)
        self.button.move(200, 200)
        self.button.resize(200, 70)

        self.button.clicked.connect(self.troubles)

    def troubles(self):
        if self.exception_type == 'usual':
            raise Exception("Что-то плохое произошло")
        elif self.exception_type == 'file':
            open('not_existing_file.txt', 'r')
