import os
import sys
import traceback

from PyQt5.QtWidgets import QApplication, QMessageBox, QMainWindow


def create_exception_handler(app: QMainWindow):
    """
    Создаём функцию перехватчик для всех ошибок

    :param app: QT приложение в котором будет выведена ошибка
    """

    def handle_exception(exc_type, exc_value, exc_traceback):
        """Обработчик для неперехваченных сообщений"""

        # Если мы пытаемся выйти через ctrl+c, то нужно просто закрыть приложение
        if issubclass(exc_type, KeyboardInterrupt):
            if app.qApp:
                app.qApp.quit()
            return

        filename, line, dummy, dummy = traceback.extract_tb(exc_traceback).pop()
        filename = os.path.basename(filename)
        error = "%s: %s" % (exc_type.__name__, exc_value)

        QMessageBox.critical(app, "Error",
                                 "<html>A critical error has occured.<br/> "
                                 + "<b>%s</b><br/><br/>" % error
                                 + "It occurred at <b>line %d</b> of file <b>%s</b>.<br/>" % (line, filename)
                                 + "</html>")

        print("Closed due to an error. This is the full error report:")
        print()
        print("".join(traceback.format_exception(exc_type, exc_value, exc_traceback)))
        sys.exit(1)

    return handle_exception


