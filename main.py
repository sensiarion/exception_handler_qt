import sys

from PyQt5.QtWidgets import QApplication

from app import Example
from exception_handler import create_exception_handler

if __name__ == '__main__':
    app = QApplication(sys.argv)

    # говорим нашему Python приложению все необработанные
    # ошибки перехватывать этой функцией

    ex = Example('file')
    sys.excepthook = create_exception_handler(ex)

    ex.show()
    sys.exit(app.exec())
